CREATE TABLE users
(
    id          bigserial primary key,
    first_name  varchar(50)  not null,
    second_name varchar(50)  not null,
    email       varchar(50)  not null unique check ( length(email) > 8 ),
    password    varchar(500) not null
);

CREATE TABLE tests
(
    id          bigserial primary key,
    test_name   varchar(50)  not null,
    description varchar(255) not null,
    owner_id    bigint       not null,
    start_date  date         not null,
    end_date    date         not null,


    constraint fk_owner
        foreign key (owner_id) references users (id) on delete cascade
);

CREATE TABLE questions
(
    id             bigserial primary key,
    question_text  varchar(255) not null,
    correct_answer varchar(255) null,
    type           varchar(20)  null,
    test_id        bigint       not null,

    constraint fk_test
        foreign key (test_id) references tests (id) on delete cascade
);

create table question_options
(
    id          bigserial primary key,
    question_id bigint  not null,
    correct     boolean not null,
    option_text varchar(255),
    constraint fk_option
        foreign key (question_id) references questions (id) on delete cascade
);