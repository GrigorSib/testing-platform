CREATE TABLE passed
(
    id          bigserial primary key,
    user_id    bigint       not null,
    test_id bigint not null,
    max_point integer,
    user_point integer,


    constraint fk_ow
        foreign key (user_id) references users (id) on delete cascade,

    constraint fk
        foreign key (test_id) references tests (id) on delete cascade
);