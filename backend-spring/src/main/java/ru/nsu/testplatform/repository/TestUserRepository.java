package ru.nsu.testplatform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.testplatform.entity.Test;
import ru.nsu.testplatform.entity.TestUser;
import ru.nsu.testplatform.entity.User;

import java.util.List;

public interface TestUserRepository extends JpaRepository<TestUser, Long> {
    TestUser findByUser(User user);
    List<TestUser> findAllByUser(User user);
    List<TestUser> findAllByTest(Test test);
}
