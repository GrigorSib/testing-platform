package ru.nsu.testplatform.entity.questions;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.testplatform.entity.identifiable.Identifiable;

import javax.persistence.Entity;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Answer extends Identifiable {

    private String answerText;

    public boolean compare(Answer userAnswer){
        return answerText.equals(userAnswer.getAnswerText());
    }
}
