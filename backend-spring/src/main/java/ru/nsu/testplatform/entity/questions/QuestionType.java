package ru.nsu.testplatform.entity.questions;

public enum QuestionType {
    ONE_OPTION("ONE_OPTION"),
    TEXT_ANSWER("TEXT_ANSWER"),
    MANY_OPTIONS("MANY_OPTIONS");

    QuestionType(String type) {
    }
}