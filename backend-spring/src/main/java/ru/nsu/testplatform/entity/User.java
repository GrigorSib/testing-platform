package ru.nsu.testplatform.entity;

import lombok.*;
import org.springframework.lang.NonNull;
import ru.nsu.testplatform.entity.identifiable.Identifiable;

import javax.persistence.*;
import java.util.List;


@Entity
@Setter
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users")
public class User extends Identifiable {
    @NonNull
    private String firstName;

    @NonNull
    private String secondName;

    @Column(unique = true)
    @EqualsAndHashCode.Include
    private String email;

    @NonNull
    private String password;

    @OneToMany
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    private List<Test> tests;
}

