package ru.nsu.testplatform.entity.questions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import ru.nsu.testplatform.entity.Test;
import ru.nsu.testplatform.entity.identifiable.Identifiable;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table(name = "questions")
public class Question extends Identifiable {
    @Enumerated(value = EnumType.STRING)
    private QuestionType type;

    private String questionText;

    private String correctAnswer;

    @ManyToOne
    @JoinColumn(name = "test_id", referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private Test test;

    @OneToMany(cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
    @JoinColumn(name = "question_id", referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<Option> options;

    public Map<Long,Boolean> getAnswerMap(){
        Map<Long,Boolean> map = new HashMap<>();
        options.forEach(o -> map.put(o.getId(),o.correct));
        return map;
    }

    public Map<Long,Boolean> getEmptyAnswerMap(){
        Map<Long,Boolean> map = new HashMap<>();
        options.forEach(o -> map.put(o.getId(),false));
        return map;
    }
}
