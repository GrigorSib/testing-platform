package ru.nsu.testplatform.controllers;

import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.testplatform.dto.TestAnswerDto;
import ru.nsu.testplatform.dto.TestDto;
import ru.nsu.testplatform.services.TestService;

@RestController
@Log
@RequiredArgsConstructor
public class TestController {

    private final TestService testService;

    @PostMapping("/test/create")
    public ResponseEntity<?> createTest(@RequestBody TestDto testDto) {
        var username = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            return ResponseEntity.ok(testService.createTest(testDto, username));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PostMapping("/test/remove")
    public ResponseEntity<?> removeTest(@RequestBody Long id) {
        var username = SecurityContextHolder.getContext().getAuthentication().getName();
        return ResponseEntity.ok(testService.removeTest(id, username));
    }


    @PostMapping("/test/edit")
    public ResponseEntity<?> editTest(@RequestBody TestDto testDto) {
        var username = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            return ResponseEntity.ok(testService.editTest(testDto, username));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }


    @PostMapping(value = "/test")
    public ResponseEntity<?> getTest(@RequestBody Long id) {
        var username = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            log.info("Found test №" + id);
            return ResponseEntity.ok(testService.getTest(id, username));
        } catch (NotFoundException e) {
            log.info("Not found test №" + id);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }


    @GetMapping("/test/all")
    public ResponseEntity<?> getUserTests() {
        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            return ResponseEntity.ok(testService.getAllTestUser(userName));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }


    @PostMapping("/test/pass")
    public ResponseEntity<?> getTestPublic(@RequestBody Long id) {
        try {
            return ResponseEntity.ok(testService.getTestNoCorrect(id));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }


    @PostMapping("/test/submit/passed")
    public ResponseEntity<?> submitPublic(@RequestBody TestAnswerDto dtoList) {
        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            var answerMap = testService.checkTest(dtoList, userName);
            return ResponseEntity.ok(answerMap);
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PostMapping("/test/responses")
    public ResponseEntity<?> getTestAnswers(@RequestBody Long id) {
        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            return ResponseEntity.ok(testService.getTestResponses(id, userName));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}
