package ru.nsu.testplatform.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.testplatform.dto.ProfileDto;
import ru.nsu.testplatform.dto.RegisterDto;
import ru.nsu.testplatform.services.AuthService;

import javax.security.auth.message.AuthException;
import javax.validation.constraints.NotBlank;

@RestController
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;

    @PostMapping("/register")
    public ResponseEntity<ProfileDto> createUser(@RequestBody RegisterDto user) {
        return ResponseEntity.ok(authService.createUser(user));
    }


    @PostMapping("/login")
    public ResponseEntity<String> auth(@NotBlank @RequestHeader("Authorization") String authorizationHeader) {
        try {
            return ResponseEntity.ok(authService.createAuthToken(authorizationHeader));
        } catch (AuthException e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
    }
}