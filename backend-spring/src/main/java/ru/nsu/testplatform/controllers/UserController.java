package ru.nsu.testplatform.controllers;

import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.testplatform.dto.ProfileDto;
import ru.nsu.testplatform.services.UserService;

@RestController
@RequiredArgsConstructor
public class UserController {
    private static final String USERS_PATH = "/user";
    private final UserService userService;

    @GetMapping(USERS_PATH + "/profile")
    public ResponseEntity<?> getUserProfile() {
        var username = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            return ResponseEntity.ok(userService.getUserProfile(username));
        } catch (
                NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PostMapping(USERS_PATH + "/edit")
    public ResponseEntity<?> editProfile(@RequestBody ProfileDto profileDto) {
        var username = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            return ResponseEntity.ok(userService.editUserProfile(username, profileDto));
        } catch (
                NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @GetMapping(USERS_PATH + "/answers")
    public ResponseEntity<?> getUserAnswers(){
        var username = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            return ResponseEntity.ok(userService.getUserAnswers(username));
        } catch (
                NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

}