package ru.nsu.testplatform.services;


import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.testplatform.dto.*;
import ru.nsu.testplatform.entity.Test;
import ru.nsu.testplatform.entity.TestUser;
import ru.nsu.testplatform.entity.questions.Option;
import ru.nsu.testplatform.entity.questions.Question;
import ru.nsu.testplatform.entity.questions.QuestionType;
import ru.nsu.testplatform.repository.TestRepository;
import ru.nsu.testplatform.repository.TestUserRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@RequiredArgsConstructor
public class TestService {
    private static final String EXIST = "TEST_NOT_EXIST";

    @PersistenceContext
    private EntityManager em;
    private final TestRepository testRepository;

    private final QuestionService questionService;
    private final UserService userService;
    private final TestUserRepository testUserRepository;

    @Transactional
    public TestDto createTest(TestDto testDto, String userLogin) throws NotFoundException {

        var user = userService.getUserByLogin(userLogin);

        var test = new Test();
        test.setTestName(testDto.getName());
        test.setDescription(testDto.getDescription());

        test.setOwner(user);

        test.setStartDate(Date.valueOf(testDto.getStartDate()));
        test.setEndDate(Date.valueOf(testDto.getEndDate()));

        List<Question> questions = new ArrayList<>();

        for (var questionDto : testDto.getQuestions()) {
            questions.add(mapQuestion(questionDto, test));
        }

        em.persist(test);

        questionService.saveAll(questions);

        test.setQuestions(questions);
        em.persist(test);
        em.clear();
        return TestDto.mapDto(test);
    }

    private Question mapQuestion(QuestionDto questionDto, Test test) {
        var question = new Question();
        question.setQuestionText(questionDto.getQuestionText());
        switch (questionDto.getType()) {
            case ONE_OPTION:
                question.setType(QuestionType.ONE_OPTION);
                var ops = new ArrayList<Option>();
                for (QuestionDto.OptionDto opt : questionDto.getOptions()) {
                    ops.add(new Option(opt.getOptionText(), opt.isCorrect(), question));
                }
                question.setOptions(ops);
                break;
            case TEXT_ANSWER:
                question.setType(QuestionType.TEXT_ANSWER);
                question.setCorrectAnswer(questionDto.getCorrectAnswer());
                break;
            case MANY_OPTIONS:
                question.setType(QuestionType.MANY_OPTIONS);
                var opts = new ArrayList<Option>();
                for (QuestionDto.OptionDto opt : questionDto.getOptions()) {
                    opts.add(new Option(opt.getOptionText(), opt.isCorrect(), question));
                }
                question.setOptions(opts);
                break;
            default:
                break;
        }
        question.setTest(test);
        return question;
    }

    @Transactional
    public TestDto editTest(TestDto testDto, String userLogin) throws NotFoundException {
        var oldTest = testRepository.findByOwnerEmailAndId(userLogin, testDto.getId())
                .orElseThrow(() -> new NotFoundException(EXIST));

        questionService.removeByTestId(oldTest.getId());
        var test = em.find(Test.class, oldTest.getId());

        test.setTestName(testDto.getName());
        test.setDescription(testDto.getDescription());

        em.persist(test);

        test.setStartDate(Date.valueOf(testDto.getStartDate()));
        test.setEndDate(Date.valueOf(testDto.getEndDate()));

        List<Question> questions = new ArrayList<>();

        for (var questionDto : testDto.getQuestions()) {
            questions.add(mapQuestion(questionDto, test));
        }


        questionService.saveAll(questions);
        test.setQuestions(questions);
        em.persist(test);
        em.clear();
        return TestDto.mapDto(test);
    }

    public TestDto getTest(Long id, String userLogin) throws NotFoundException {
        var test = testRepository.findByOwnerEmailAndId(userLogin, id)
                .orElseThrow(() -> new NotFoundException(EXIST));

        return TestDto.mapDto(test);
    }


    public Map<Long, Boolean> checkTest(TestAnswerDto dtoList, String login) throws NotFoundException {
        var user = userService.getUserByLogin(login);

        var checkList = new HashMap<Long, Boolean>();
        var answerList = dtoList.getAnswerDtos();
        Test test = null;
        if (!answerList.isEmpty()) {
            test = questionService.getQuestionById(answerList.get(0).getQuestionId()).getTest();
        }
        Long a = null;
        for (var answer : answerList) {
            var question = questionService.getQuestionById(answer.getQuestionId());
            switch (question.getType()) {
                case ONE_OPTION:
                    Long idAnswer = Long.parseLong(answer.getAnswerText());
                    var options = question.getOptions();
                    var mark = new AtomicBoolean(false);
                    options.forEach(ops -> {
                        if (idAnswer.equals(ops.getId())) {
                            mark.set(ops.isCorrect());
                        }
                    });
                    checkList.put(question.getId(), mark.get());
                    break;
                case TEXT_ANSWER:
                    boolean markT = answer.getAnswerText().equals(question.getCorrectAnswer());
                    checkList.put(question.getId(), markT);
                    break;
                case MANY_OPTIONS:
                    var parced = answer.getAnswerText().split(" ");
                    Map<Long, Boolean> userAnswerMap = question.getEmptyAnswerMap();
                    for (String s : parced) {
                        a = Long.parseLong(s);
                        userAnswerMap.replace(a, true);
                    }
                    var rightAnswerMap = question.getAnswerMap();
                    boolean markM = userAnswerMap.equals(rightAnswerMap);
                    checkList.put(question.getId(), markM);
                    break;
                default:
                    break;
            }
        }

        var maxPoints = checkList.size();
        var userPoints = new AtomicInteger();
        checkList.forEach((k, v) -> {
            if (Boolean.TRUE.equals(v)) {
                userPoints.getAndIncrement();
            }
        });
        var result = testUserRepository.findByUser(user);
        if (result == null) {
            result = new TestUser();
            result.setUser(user);
            result.setTest(test);
        }
        result.setMaxPoint(maxPoints);
        result.setUserPoint(userPoints.get());
        testUserRepository.save(result);

        return checkList;
    }


    public List<ShortTestDto> getAllTestUser(String email) throws NotFoundException {
        var user = userService.getUserByLogin(email);

        var tests = testRepository.findAllByOwner(user);

        List<ShortTestDto> dtoList = new ArrayList<>();
        tests.forEach(a -> dtoList.add(ShortTestDto.mapDto(a)));

        if (dtoList.size() < 1) {
            return null;
        } else {
            return dtoList;
        }
    }

    public TestDto getTestNoCorrect(Long id) throws NotFoundException {
        var test = testRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(EXIST));

        return TestDto.mapDtoNoCorrect(test);
    }

    public List<TestResultDto> getTestResponses(Long id, String userLogin) throws NotFoundException {
        var test = testRepository.findByOwnerEmailAndId(userLogin, id)
                .orElseThrow(() -> new NotFoundException(EXIST));

        var testsUser = testUserRepository.findAllByTest(test);
        List<TestResultDto> resultDtos = new ArrayList<>();
        for (TestUser t : testsUser) {
            resultDtos.add(TestResultDto.mapDto(t));
        }
        return resultDtos;
    }


    @Transactional
    public String removeTest(Long id, String username) {
        testRepository.removeByOwnerEmailAndId(username, id);
        return "Done";
    }
}
