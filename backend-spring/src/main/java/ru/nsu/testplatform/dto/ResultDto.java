package ru.nsu.testplatform.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.testplatform.entity.TestUser;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ResultDto {
    private String name;
    private String description;
    private String result;

    public static ResultDto mapDto(TestUser testUser){
        var res = new ResultDto();
        res.setDescription(testUser.getTest().getDescription());
        res.setName(testUser.getTest().getTestName());
        res.setResult(testUser.getUserPoint() + "/" + testUser.getMaxPoint());

        return res;
    }
}
