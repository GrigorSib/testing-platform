package ru.nsu.testplatform.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.nsu.testplatform.entity.questions.Question;
import ru.nsu.testplatform.entity.questions.QuestionType;

import java.util.ArrayList;
import java.util.List;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
public class QuestionDto {

    private final Long id;
    private final QuestionType type;
    private final String questionText;
    private final String correctAnswer;
    private final List<OptionDto> options;

    public static QuestionDto mapDto(Question question) {
        List<OptionDto> ops = null;
        if (question.getType() == QuestionType.ONE_OPTION || question.getType() == QuestionType.MANY_OPTIONS) {
            ops = new ArrayList<>();
            for (var option : question.getOptions()) {
                ops.add(new OptionDto(option.getId(),option.getOptionText(), option.isCorrect()));
            }
        }

        return new QuestionDto(question.getId(),question.getType(), question.getQuestionText(), question.getCorrectAnswer(), ops);
    }

    public static QuestionDto mapDtoNoCorrect(Question question) {
        List<OptionDto> ops = null;
        if (question.getType() == QuestionType.ONE_OPTION || question.getType() == QuestionType.MANY_OPTIONS) {
            ops = new ArrayList<>();
            for (var option : question.getOptions()) {
                ops.add(new OptionDto(option.getId(),option.getOptionText(), false));
            }
        }

        return new QuestionDto(question.getId(),question.getType(), question.getQuestionText(), "I don't know", ops);
    }

    @Getter
    @AllArgsConstructor
    @EqualsAndHashCode
    public static class OptionDto {
        Long id;
        String optionText;
        boolean correct;
    }
}