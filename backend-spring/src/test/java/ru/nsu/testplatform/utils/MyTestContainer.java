package ru.nsu.testplatform.utils;

import org.testcontainers.containers.PostgreSQLContainer;

public class MyTestContainer extends PostgreSQLContainer<MyTestContainer> {
    private static final String IMAGE_VERSION = "postgres:13.2";
    private static MyTestContainer container;

    private MyTestContainer() {
        super(IMAGE_VERSION);
    }

    public static MyTestContainer getInstance() {
        if (container == null) {
            container = new MyTestContainer();
        }
        return container;
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("DB_URL", container.getJdbcUrl());
        System.setProperty("DB_USERNAME", container.getUsername());
        System.setProperty("DB_PASSWORD", container.getPassword());
    }

    @Override
    public void stop() {
        //do nothing, JVM handles shut down
    }
}