package ru.nsu.testplatform;

import com.google.gson.Gson;
import javassist.NotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.nsu.testplatform.dto.RegisterDto;
import ru.nsu.testplatform.services.AuthService;
import ru.nsu.testplatform.services.UserService;
import ru.nsu.testplatform.utils.MyTestContainer;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@Testcontainers
class AuthServiceTests {

    @Container
    private static final PostgreSQLContainer<MyTestContainer> postgreSQLContainer = MyTestContainer.getInstance();
    private final Gson gson = new Gson();
    @Autowired
    private AuthService authService;
    @Autowired
    private UserService userService;

    @Test
    void creationUserTest() throws NotFoundException {
        String jsonDto = "{\n" +
                "    \"firstName\": \"Alexander\",\n" +
                "    \"secondName\": \"Sidorov\",\n" +
                "    \"email\": \"a.sidorov@g.nsu.ru\",\n" +
                "    \"password\": \"secret\"\n" +
                "}";

        var regDto = gson.fromJson(jsonDto, RegisterDto.class);

        var userExpected = authService.createUser(regDto);
        assertEquals(userExpected, userService.getUserProfile(userExpected.getEmail()));
    }

    void createUser() {
        String jsonDto = "{\n" +
                "    \"firstName\": \"Alexander\",\n" +
                "    \"secondName\": \"J\",\n" +
                "    \"email\": \"a.sidorov1@g.nsu.ru\",\n" +
                "    \"password\": \"secret\"\n" +
                "}";

        var regDto = gson.fromJson(jsonDto, RegisterDto.class);
        authService.createUser(regDto);
    }


    @Test
    void loginUserTest() {
        createUser();
        String jsonDto = "Basic YS5zaWRvcm92MUBnLm5zdS5ydTpzZWNyZXQ=";
        assertDoesNotThrow(()-> authService.createAuthToken(jsonDto));
    }
}
