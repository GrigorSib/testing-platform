package ru.nsu.testplatform;

import com.google.gson.Gson;
import javassist.NotFoundException;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.event.annotation.BeforeTestExecution;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.nsu.testplatform.dto.RegisterDto;
import ru.nsu.testplatform.dto.TestAnswerDto;
import ru.nsu.testplatform.dto.TestDto;
import ru.nsu.testplatform.repository.TestRepository;
import ru.nsu.testplatform.services.AuthService;
import ru.nsu.testplatform.services.TestService;
import ru.nsu.testplatform.utils.MyTestContainer;

import java.sql.Date;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Testcontainers
public class TestServiceTest {
    @Container
    private static final PostgreSQLContainer<MyTestContainer> postgreSQLContainer = MyTestContainer.getInstance();
    private static final Gson gson = new Gson();
    @Autowired
    private TestService testService;

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private AuthService authService;


    public void createUser(String userName) {
        String jsonDto = "{\n" +
                "    \"firstName\": \"Alexander\",\n" +
                "    \"secondName\": \"J\",\n" +
                "    \"email\": "+ userName+ ",\n" +
                "    \"password\": \"secret\"\n" +
                "}";

        var regDto = gson.fromJson(jsonDto, RegisterDto.class);
        authService.createUser(regDto);
    }


    @Test
    void createTest() throws NotFoundException {
        createUser("a.yakobson@g.nsu.ru");
        String jsonDto = "{\n" +
                "\t\"name\": \"Title\", \n" +
                "\t\"description\": \"Description\", \n" +
                "\t\"startDate\": \"2021-03-10\",\n"+
                "\t\"endDate\": \"2022-03-10\",\n"+
                "\t\"questions\": [\n" +
                "\t\t{\n" +
                "\t\t\t\"type\": \"ONE_OPTION\",\n" +
                "\t\t\t\"questionText\": \"Ur good?\", \n" +
                "\t\t\t\"options\": [\n" +
                "\t\t\t\t{\n" +
                "\t\t\t\t\t\"optionText\": \"Yes\",\n" +
                "\t\t\t\t\t\"correct\": true\n" +
                "\t\t\t\t},\n" +
                "\t\t\t\t{\n" +
                "\t\t\t\t\t\"optionText\": \"No\",\n" +
                "\t\t\t\t\t\"correct\": false\n" +
                "\t\t\t\t}\n" +
                "\t\t\t]\t\t\t\n" +
                "\t\t}, \n" +
                "\t\t{\n" +
                "\t\t\t\"type\": \"TEXT_ANSWER\", \n" +
                "\t\t\t\"questionText\": \"How do u do?\",\n" +
                "\t\t\t\"correctAnswer\": \"ok\"\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"type\": \"MANY_OPTIONS\",\n" +
                "\t\t\t\"questionText\": \"What is the capital of ..\",\n" +
                "\t\t\t\"options\": [\n" +
                "\t\t\t\t{\n" +
                "\t\t\t\t\t\"optionText\": \"America\",\n" +
                "\t\t\t\t\t\"correct\": true\n" +
                "\t\t\t\t},\n" +
                "\t\t\t\t{\n" +
                "\t\t\t\t\t\"optionText\": \"LA\",\n" +
                "\t\t\t\t\t\"correct\": true\n" +
                "\t\t\t\t},\n" +
                "\t\t\t\t{\n" +
                "\t\t\t\t\t\"optionText\": \"Japan\",\n" +
                "\t\t\t\t\t\"correct\": false\n" +
                "\t\t\t\t}\n" +
                "\t\t\t]\n" +
                "\t\t}\n" +
                "\t]\n" +
                "}";

        var testDto = gson.fromJson(jsonDto, TestDto.class);

        var test = testService.createTest(testDto, "a.yakobson@g.nsu.ru");

        assertEquals(test, testService.getTest(test.getId(), "a.yakobson@g.nsu.ru"));
    }


    public TestDto prepareTest(String userName) throws NotFoundException {
        String jsonDto = "{\n" +
                "\t\"name\": \"Title\", \n" +
                "\t\"description\": \"Description\", \n" +
                "\t\"startDate\": \"2021-03-10\",\n"+
                "\t\"endDate\": \"2022-03-10\",\n"+
                "\t\"questions\": [\n" +
                "\t\t{\n" +
                "\t\t\t\"type\": \"ONE_OPTION\",\n" +
                "\t\t\t\"questionText\": \"Ur good?\", \n" +
                "\t\t\t\"options\": [\n" +
                "\t\t\t\t{\n" +
                "\t\t\t\t\t\"optionText\": \"Yes\",\n" +
                "\t\t\t\t\t\"correct\": true\n" +
                "\t\t\t\t},\n" +
                "\t\t\t\t{\n" +
                "\t\t\t\t\t\"optionText\": \"No\",\n" +
                "\t\t\t\t\t\"correct\": false\n" +
                "\t\t\t\t}\n" +
                "\t\t\t]\t\t\t\n" +
                "\t\t}, \n" +
                "\t\t{\n" +
                "\t\t\t\"type\": \"TEXT_ANSWER\", \n" +
                "\t\t\t\"questionText\": \"How do u do?\",\n" +
                "\t\t\t\"correctAnswer\": \"ok\"\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"type\": \"MANY_OPTIONS\",\n" +
                "\t\t\t\"questionText\": \"What is the capital of ..\",\n" +
                "\t\t\t\"options\": [\n" +
                "\t\t\t\t{\n" +
                "\t\t\t\t\t\"optionText\": \"America\",\n" +
                "\t\t\t\t\t\"correct\": true\n" +
                "\t\t\t\t},\n" +
                "\t\t\t\t{\n" +
                "\t\t\t\t\t\"optionText\": \"LA\",\n" +
                "\t\t\t\t\t\"correct\": true\n" +
                "\t\t\t\t},\n" +
                "\t\t\t\t{\n" +
                "\t\t\t\t\t\"optionText\": \"Japan\",\n" +
                "\t\t\t\t\t\"correct\": false\n" +
                "\t\t\t\t}\n" +
                "\t\t\t]\n" +
                "\t\t}\n" +
                "\t]\n" +
                "}";
        var testDto = gson.fromJson(jsonDto, TestDto.class);

        return testService.createTest(testDto, userName);
    }

    @Test
    void checkTest() throws NotFoundException {
        String user = "a.yakobson1@g.nsu.ru";
        createUser(user);
        var test = prepareTest(user);
        String answers = "{\n" +
                "\"answerDtos\":[\n" +
                "\t{\n" +
                "\t\t\"questionId\": 1,\n" +
                "\t\t\"answerText\": \"1\" \n" +
                "\t},\n" +
                "\t{ \n" +
                "\t\t\"questionId\": 3,\n" +
                "\t\t\"answerText\": \"3 4\"\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\t\"questionId\": 2,\n" +
                "\t\t\"answerText\": \"ok\"\n" +
                "\t}\n" +
                "]\n" +
                "}";

        var testAnswerDto = gson.fromJson(answers, TestAnswerDto.class);
        var rightAnswers = new HashMap<Long,Boolean>();
        rightAnswers.put(1L, true);
        rightAnswers.put(3L, true);
        rightAnswers.put(2L, true);
        var answerMap = testService.checkTest(testAnswerDto,user);
        assertEquals(rightAnswers,answerMap);
    }


    @Test
    void editTestTest() throws NotFoundException {
        String user = "a.yakobson2@g.nsu.ru";
        createUser(user);
        var testDto = prepareTest(user);
        var test = testRepository.findById(testDto.getId()).orElseThrow();

        test.setTestName("New Test NaMe");
        test.setDescription("New Test DesCriPtiOn");

        var newTestDto = testService.editTest(TestDto.mapDto(test),user);

        assertNotEquals(newTestDto,testDto);
        assertEquals(newTestDto.getName(),"New Test NaMe");
        assertEquals(newTestDto.getDescription(),"New Test DesCriPtiOn");
    }


    @Test
    void removeTestTest() throws NotFoundException {
        String user = "a.yakobson3@g.nsu.ru";
        createUser(user);
        var testDto = prepareTest(user);
        assertEquals("Done", testService.removeTest(testDto.getId(),user));
    }

    @Test
    void getTestAnswersTest() throws NotFoundException {
        String user = "a.yakobson4@g.nsu.ru";
        createUser(user);
        assertNull(testService.getAllTestUser(user));
        var testDto = prepareTest(user);

        String answers = "{\n" +
                "\"answerDtos\":[\n" +
                "\t{\n" +
                "\t\t\"questionId\": 1,\n" +
                "\t\t\"answerText\": \"1\" \n" +
                "\t},\n" +
                "\t{ \n" +
                "\t\t\"questionId\": 3,\n" +
                "\t\t\"answerText\": \"3 4\"\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\t\"questionId\": 2,\n" +
                "\t\t\"answerText\": \"ok\"\n" +
                "\t}\n" +
                "]\n" +
                "}";
        var testAnswerDto = gson.fromJson(answers, TestAnswerDto.class);

        createUser("a.sidorov6@g.nsu.ru");
        createUser("a.sidorov7@g.nsu.ru");
        testService.checkTest(testAnswerDto,"a.sidorov6@g.nsu.ru" );
        testService.checkTest(testAnswerDto,"a.sidorov7@g.nsu.ru" );

        assertEquals(2,testService.getTestResponses(testDto.getId(),"a.yakobson4@g.nsu.ru").size());


    }


}
