import React, {useState} from "react";
import {FormGroup, Grid, Icon, Paper, Typography} from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

export function QuestionManyOptions(props) {
    const [value, setValue] = useState([]);
    const {id, index, questionText, options, updateQuestionSubmit, disabled, correct} = props

    let handleRadioChange = (option_id_str) => {
        let option_id = parseInt(option_id_str)
        if (value.includes(option_id)) {
            const index = value.indexOf(option_id)
            value.splice(index, 1)
        } else {
            value.push(option_id)
        }
        setValue(value)
        updateQuestionSubmit?.(value, id)
    };

    let color
    if (correct === undefined) {
        color = undefined
    } else {
        color = correct ? "green" : "red"
    }

    return <Grid key={id}>
        <Paper style={{marginTop: 20}}>
            <div style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginLeft: '6px',
                paddingTop: '15px',
                paddingBottom: '15px'
            }}>
                {color && <Icon style={{background:color}}></Icon>}
                <Typography variant="subtitle1"
                            style={{marginLeft: '10px'}}>{index + 1}. {questionText}</Typography>

                <FormGroup aria-label="quiz" name="quiz" onChange={(e) => {
                    handleRadioChange(e.target.value)
                }}>
                    {options.map((option) => (
                        <div key={option.id}>
                            <div style={{display: 'flex', marginLeft: '7px'}}>
                                <FormControlLabel value={option.id} control={
                                    disabled ?
                                        <Checkbox color="primary" disabled={true} checked={option.correct}/> :
                                        <Checkbox color="primary"/>
                                } label={option.optionText}/>
                            </div>
                        </div>
                    ))}
                </FormGroup>
            </div>
        </Paper>
    </Grid>
}