import React, {useEffect, useState} from 'react';
import {AppBar, CircularProgress, Grid, Paper, Typography} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {getQuestionByType} from "../utils";
import Toolbar from "@material-ui/core/Toolbar";
import axios from "axios";
import {api} from "../../../lib/api";
import {routes, UiState} from "../../utils";
import style from "../../App/App.module.scss";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import {NavLink} from "react-router-dom";

export default function TestSubmit(props) {
    const id = props.match.params.id
    const [testData, setTestData] = useState();
    const [answers, _] = useState(new Map())
    const [state, setState] = useState(UiState.Ready)
    const [error, setError] = useState(null)
    const [checks, setChecks] = useState(null)
    const [open, setOpen] = useState(true);
    const [testOutdated, setTestOutdated] = useState(false)
    const [jwt, setJwt] = useState(null)

    useEffect(getTestData, [])

    useEffect(() => {
        if (testData && !isTestDateValid()) {
            setTestOutdated(true)
            setTimeout(() => alert("Сегодня не день сдачи теста"), 20)
        }
    }, [testData])

    const dispatchError = (error) => {
        if (error?.response) {
            console.log(error?.response)
        }
        if (error?.response?.status === 404) {
            setError("Такого теста не существует :(")
        } else {
            setError(error.toString())//todo
        }
        setState(UiState.Error)
        console.log(error)
    }

    const dispatchLoading = () => {
        setState(UiState.Loading)
    }

    const dispatchReady = () => {
        setState(UiState.Ready)
    }

    function setJwtToken(jwtToken) {
        setJwt(jwtToken)
        setOpen(false)
    }

    async function getTestData() {
        try {
            dispatchLoading()
            const formData = await axios.post(api.getPassTestById, id, {
                headers: {
                    'content-type': 'application/json'
                }
            }).then((r) => {
                console.log(r)
                return r.data
            })
            // formData.questions
            formData.questions.map((ques, i)=> {
                ques.correctAnswer = null
            })
            let formData1 = {...formData, questions: uniq(formData.questions)}
            console.log({formData1})
            setTestData(formData1)
            dispatchReady()
        } catch (e) {
            dispatchError(e)
        }
    }

    function uniq(a) {
        var seen = [];
        return a.filter(function(item) {
            if (seen.includes(item.id)) {
                return false
            } else {
                seen.push(item.id)
                return true
            }
        });
    }

    function isTestDateValid() {
        const {startDate, endDate} = testData
        let now = new Date()
        let start = new Date(startDate)
        let end = new Date(endDate)
        return (now.getTime() >= start.getTime() && now.getTime() <= end.getTime())
    }

    const updateQuestionSubmit = (answer, questionId) => {
        answers.set(questionId, {questionId: questionId, answer: answer})
        console.log(answers)
    }

    const getAnswer = (answer) => {
        if (Array.isArray(answer)) {
            var result = ""
            answer.forEach((value, index) => {
                if (index === answer.length - 1) {
                    result += `${value}`
                } else {
                    result += `${value} `
                }
            })
            return result
        } else {
            return answer?.toString()
        }
    }

    const onSubmit = async () => {
        try {
            dispatchLoading()
            const form = Array.from(answers, ([_, value]) => (
                {
                    questionId: value.questionId,
                    answerText: getAnswer(value.answer),
                }
            ))
            console.log({jwt})
            let submitResult = await axios.post(api.submitTest, {
                answerDtos: form
            }, {
                headers: {
                    Authorization: 'Bearer ' + jwt.data
                }
            }).then(
                (r) => {
                    console.log(r)
                    return r.data
                }
            )
            setChecks(submitResult)
            dispatchReady()
        } catch (e) {
            dispatchError(e)
        }
    }

    return (
        <React.Fragment>
            {state !== UiState.Ready &&
            <div className={style.modal}>
                {
                    state === UiState.Loading && <CircularProgress color={"secondary"}/>
                }
                {
                    state === UiState.Error && <div>
                        <div className={style.close} onClick={() => dispatchReady()}>
                        </div>
                        <div className={style.error}>
                            {error}
                        </div>
                    </div>
                }
            </div>
            }
            <AppBar position="static" style={{backgroundColor: 'teal', marginBottom: "20px"}}>
                <Toolbar>
                    <Typography variant="h6">
                        Прохождение теста
                    </Typography>
                </Toolbar>
            </AppBar>
            {open && testData && !testOutdated &&
            <LoginFormDialog
                setJwtToken={setJwtToken}
            />
            }
            {testData && <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
            >
                <Grid item xs={12} sm={5} style={{width: '100%'}}>
                    <Grid style={{borderTop: '10px solid teal', borderRadius: 10}}>
                        <Paper elevation={2} style={{width: '100%'}}>
                            <div style={{
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'flex-start',
                                marginLeft: '15px',
                                paddingTop: '20px',
                                paddingBottom: '20px'
                            }}>
                                <Typography variant="h4"
                                            style={{fontFamily: 'sans-serif Roboto', marginBottom: "15px"}}>
                                    {testData?.name}
                                </Typography>
                                <Typography variant="subtitle1">{testData?.description}</Typography>
                            </div>
                        </Paper>
                    </Grid>

                    {testData?.questions.map((ques, i) => {
                        if (checks === null) {
                            return getQuestionByType(false, ques, i, updateQuestionSubmit)
                        } else {
                            let correct = checks[ques.id] ? checks[ques.id] : false
                            return getQuestionByType(true, ques, i, null, correct,)
                        }
                    })}

                    {!testOutdated && checks === null &&
                    <div style={{display: 'flex', marginBottom: 20, marginTop: 20}}>
                        <Button variant="contained" color="primary" onClick={() => onSubmit()}>
                            Завершить
                        </Button>
                    </div>}
                </Grid>
            </Grid>}
        </React.Fragment>
    )
}

function LoginFormDialog(props) {
    const {setJwtToken} = props
    const [loading, setLoading] = useState(false)

    const saveLogin = (jwtToken) => {
        setJwtToken(jwtToken)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            setLoading(true)
            const data = new FormData(event.target);
            const loginForm = {
                email: data.get('email'),
                password: data.get('password')
            }
            const {email, password} = loginForm
            console.log(loginForm);
            const base64Authorization = btoa(`${email}:${password}`);
            let response = await axios.post(api.login, null, {
                headers: {
                    'Authorization': 'Basic ' + base64Authorization,
                }
            })
            saveLogin(response)
            console.log(response)
        } catch (e) {
            alert("Неправильный логин или пароль")
        }
        setLoading(false)
    }


    return (
        <div>
            <Dialog open={true} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Вход</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Войдите, чтобы начать тестирование
                    </DialogContentText>
                    {/*{loading &&
                        <CircularProgress/>
                    }*/}
                    <form onSubmit={handleSubmit}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Логин"
                            name="email"
                            autoComplete="email"
                            autoFocus
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Пароль"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                        />
                        <div style={{marginTop: "20px", marginBottom: "10px"}}>
                            <Button
                                type="submit"
                                variant="contained"
                                color="primary"
                                style={{marginRight: "10px"}}
                            >
                                Войти
                            </Button>
                            <Button type="button" variant={"outlined"} color="primary" component={NavLink}
                                    target="_blank"
                                    to={routes.signup}>
                                Зарегистрироваться
                            </Button>
                        </div>
                    </form>
                </DialogContent>
            </Dialog>
        </div>
    );
}