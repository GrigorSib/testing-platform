import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import CardMedia from "@material-ui/core/CardMedia";

const useStyles = makeStyles((theme) => ({
    toolbar: {
        minHeight: 70,
        textAlign: "center",
        alignItems: 'flex-start',
        paddingTop: theme.spacing(1),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    }
}));

export function ResponsesTab(props) {
    const classes = useStyles();
    const {responses} = props

    return (
        <Container className={classes.cardGrid} maxWidth="md">
            {responses && responses.length !== 0 ? <Grid container spacing={4}>
                    {responses?.map((response) => {
                        return <Grid item xs={12} sm={6} md={4} style={{marginBottom: "20px"}}>
                            <Card className={classes.card} variant={"outlined"}>
                                <CardMedia
                                    className={classes.cardMedia}
                                    image="https://source.unsplash.com/random?person,student"
                                    title="Image title"
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        Email: {response.email}
                                    </Typography>
                                    <Typography>
                                        Результат: {response.result}
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                    })}
                </Grid> :
                <Typography gutterBottom variant="h5" component="h2" style={{marginLeft: "25%"}}>
                    Ещё никто не ответил на тест :(
                </Typography>
            }
        </Container>
    )
}