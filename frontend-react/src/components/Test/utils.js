import {questionTypes} from "./types";
import {QuestionOneOption} from "./questions/QuestionOneOption";
import {QuestionManyOptions} from "./questions/QuestionManyOptions";
import {QuestionTextAnswer} from "./questions/QuestionTextAnswer";
import React from "react";

export function getQuestionByType(disabled, question, index, updateQuestionSubmit, isCorrect, correctAnswer) {
    const {type} = question
    if (type === questionTypes.oneOption) {
        return <QuestionOneOption
            index={index}
            id={question.id}
            questionText={question.questionText}
            options={question.options}
            updateQuestionSubmit={updateQuestionSubmit}
            disabled={disabled}
            correct={isCorrect}
            // correctAnswer={correctAnswer}
        />
    } else if (type === questionTypes.manyOptions) {
        return <QuestionManyOptions
            index={index}
            id={question.id}
            questionText={question.questionText}
            options={question.options}
            updateQuestionSubmit={updateQuestionSubmit}
            disabled={disabled}
            correct={isCorrect}
            // correctAnswer={correctAnswer}
        />
    } else if (type === questionTypes.textAnswer) {
        return <QuestionTextAnswer
            index={index}
            id={question.id}
            questionText={question.questionText}
            correctAnswer={question.correctAnswer}
            updateQuestionSubmit={updateQuestionSubmit}
            disabled={disabled}
            correct={isCorrect}
        />
    } else {
        return null//todo: show error?
    }
}

export function getFormattedDate(date) {
    if (date === null || date === undefined) {
        return null
    }
    var dateFormat = require('dateformat');
    return dateFormat(date, "yyyy-mm-dd");
}