import React, {createRef, useEffect, useState} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {NavLink} from "react-router-dom";
import {localStorageKeys, routes, UiState} from "../utils";
import {UserTestGrid} from "../Test/other/UserTestGrid";
import {UserMenu} from "../user/UserMenu";
import axios from "axios";
import {api} from "../../lib/api";
import style from "../App/App.module.scss";
import {CircularProgress} from "@material-ui/core";
import {getJwt} from "../../lib/storage";

const useStyles = makeStyles((theme) => ({
    title: {
        flexGrow: 1
    },
    userIcon: {
        marginRight: theme.spacing(2),
        height: 40,
        width: 40
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
    center: {
        margin: 'auto',
        width: '50%',
        padding: '10px',
        //todo: not centered
    },
}));

export default function Main() {
    const classes = useStyles();

    const jwtToken = getJwt()
    const tokenExists = jwtToken != null
    const [authenticated, setAuthenticated] = useState(tokenExists)
    const [tests, setTests] = useState([])

    const [uiState, setUiState] = useState(UiState.Ready)
    const [error, setError] = useState(null)

    const dispatchError = (error) => {
        if (error?.response) {
            console.log(error?.response)
        }
        setError(error.toString())//todo
        setUiState(UiState.Error)
        console.log(error)
    }

    const dispatchLoading = () => {
        setUiState(UiState.Loading)
    }

    const dispatchReady = () => {
        setUiState(UiState.Ready)
    }

    const fetchTests = async () => {
        if (!authenticated) {
            return
        }
        try {
            dispatchLoading()
            const tests = await axios.get(api.getTests, {
                headers: {
                    Authorization: 'Bearer ' + jwtToken
                }
            }).then((r)=> {
                console.log(r)
                return r.data
            })
            if (tests === null || tests.length === 0) {
                setTests([])
            } else {
                setTests(tests)
            }
            dispatchReady()
        } catch (e) {
            dispatchError(e)
        }
    }
    useEffect(fetchTests, [])

    return (
        <React.Fragment>
            <CssBaseline/>
            {uiState !== UiState.Ready &&
            <div className={style.modal}>
                {
                    uiState === UiState.Loading && <CircularProgress color={"secondary"}/>
                }
                {
                    uiState === UiState.Error && <div>
                        <div className={style.close} onClick={() => dispatchReady()}>
                        </div>
                        <div className={style.error}>
                            {error}
                        </div>
                    </div>
                }
            </div>
            }
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" className={classes.title} color="inherit">
                        Testing Platform
                    </Typography>
                    {
                        !authenticated ?
                            <div>
                                <Button color="inherit" component={NavLink} to={routes.signin}>Войти</Button>
                                <Button color="inherit" component={NavLink}
                                        to={routes.signup}>Зарегистрироваться</Button>
                            </div> : <UserMenu state={{authenticated: setAuthenticated}}/>
                    }
                </Toolbar>
            </AppBar>
            <main>
                {/* Hero unit */}
                <div className={classes.heroContent}>
                    <Container maxWidth="sm">
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            Testing Platform
                        </Typography>
                        <Typography variant="h5" align="center" color="textSecondary" paragraph>
                            Создание и написание тестов стало проще
                        </Typography>
                        <div className={classes.heroButtons}>
                            {authenticated ? <Grid container spacing={2} justify="center">
                                    <Grid item>
                                        <Button component={NavLink} to={{
                                            pathname: routes.testView,
                                            search: "edit=true"
                                        }}
                                                variant="contained"
                                                color="primary">
                                            Создать тест
                                        </Button>
                                    </Grid>
                                    <Grid item>
                                        <Button component={NavLink} to={routes.userTestAnswers}
                                                variant="outlined" color="primary">
                                            Посмотреть мои ответы
                                        </Button>
                                    </Grid>
                                </Grid> :
                                <Button component={NavLink} to={routes.signin} par
                                        variant="contained" color="primary" className={classes.center}>
                                    Начать пользоваться
                                </Button>
                            }
                        </div>
                    </Container>
                </div>
                {authenticated &&
                <UserTestGrid
                    tests={tests}
                />
                }
            </main>
            {/* Footer */}
           {/* <footer className={classes.footer}>
                <Typography variant="h6" align="center" gutterBottom>
                    Footer
                </Typography>
                <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
                    Something here to give the footer a purpose!
                </Typography>
            </footer>*/}
            {/* End footer */}
        </React.Fragment>
    );
}