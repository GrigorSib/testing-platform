import Main from "../Main/Main";
import SignUp from "../SignUp/SignUp";
import {HashRouter, Switch, Route} from 'react-router-dom';
import SignIn from "../SignIn/SignIn";
import {routes} from "../utils"
import TestSubmit from "../Test/submit/TestSubmit";
import {TestView} from "../Test/viewtest/TestView";
import Profile from "../user/Profile";
import {TestAnswers} from "../Test/answers/TestAnswers";

function App() {
    return (
        <div>
            <HashRouter>
                <Switch>
                    <Route path={routes.main} exact component={Main}/>
                    <Route path={routes.signup} exact component={SignUp}/>
                    <Route path={routes.signin} exact component={SignIn}/>
                    <Route path={routes.testView} exact component={TestView}/>
                    <Route path={routes.testSubmit + "/:id"} exact component={TestSubmit}/>
                    <Route path={routes.profile} exact component={Profile}/>
                    <Route path={routes.userTestAnswers} exact component={TestAnswers}/>
                </Switch>
            </HashRouter>
        </div>
    );
}

export default App;