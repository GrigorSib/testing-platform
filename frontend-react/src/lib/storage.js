import {localStorageKeys} from "../components/utils";
import Cookies from "js-cookie"

function saveJwt(jwt, remember) {
    if (remember) {
        Cookies.set(localStorageKeys.jwtToken, jwt)
    } else {
        sessionStorage.setItem(localStorageKeys.jwtToken, jwt)
    }
    // sessionStorage.setItem(localStorageKeys.rememberMe, remember)
}

function removeJwt() {
    Cookies.remove(localStorageKeys.jwtToken)
    sessionStorage.removeItem(localStorageKeys.jwtToken)
    // if (sessionStorage.getItem(localStorageKeys.rememberMe)) {
    //     sessionStorage.removeItem(localStorageKeys.jwtToken)
    // } else {
    //     Cookies.remove(localStorageKeys.jwtToken)
    // }
}

function getJwt() {
    let jwt = Cookies.get(localStorageKeys.jwtToken)
    if (!jwt) {
        jwt = sessionStorage.getItem(localStorageKeys.jwtToken)
    }
    // console.log(jwt)
    return jwt
    // if (sessionStorage.getItem(localStorageKeys.rememberMe)) {
    //     jwt = sessionStorage.getItem(localStorageKeys.jwtToken)
    // } else {
    //     jwt = Cookies.get(localStorageKeys.jwtToken)
    // }
}

export {saveJwt, removeJwt, getJwt}