// export const host = "http://localhost:8080"
export const host = ""


export const api = {
    register: `${host}/register`,
    login: `${host}/login`,
    submitTest: `${host}/test/submit/passed`,
    userTestAnswers: `${host}/user/answers`,
    testResponses: `${host}/test/responses`,
    getPassTestById: `${host}/test/pass`,
    getTests: `${host}/test/all`,
    getTestById: `${host}/test`,
    removeTest: `${host}/test/remove`,
    createTest: `${host}/test/create`,
    editTest: `${host}/test/edit`,
    profileEdit: `${host}/user/edit`,
    profileGet: `${host}/user/profile`,
}